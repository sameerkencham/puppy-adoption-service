import { getGreeting } from '../support/app.po';

describe('puppys-are-cute', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to puppys-are-cute!');
  });
});
