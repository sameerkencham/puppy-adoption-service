module.exports = {
  name: 'puppys-are-cute',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/puppys-are-cute',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
