import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PuppysService } from '../puppys.service';
import { DataShareService } from "../data-share.service";
@Component({
  selector: 'operation-puppys-pet-edit',
  templateUrl: './pet-edit.component.html',
  styleUrls: ['./pet-edit.component.css']
})
export class PetEditComponent implements OnInit {
  puppyEdit: any = {

  };
  angForm: FormGroup;

  constructor(private route: ActivatedRoute,
    private bs: PuppysService,
    private fb: FormBuilder, private puppy: DataShareService) {
    this.createForm();
  }
  createForm() {
    this.angForm = this.fb.group({
      breed: ['', Validators.required],
      age: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.puppy.currentPuppy.subscribe(res => {
      console.log(res);
      this.puppyEdit = res
    })
  }

  updatePuppy(breed, age, description, id) {
    console.log(breed);
    this.bs.updatePuppy(breed, age, description, id);

  }

}
