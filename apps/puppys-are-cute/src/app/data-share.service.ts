import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataShareService {
  private puppySource = new BehaviorSubject(null);
  currentPuppy = this.puppySource.asObservable();

  constructor() { }

  changePuppy(puppy: any) {
    this.puppySource.next(puppy)
  }
}
