import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AppComponent } from './app.component';
import { PetAddComponent } from './pet-add/pet-add.component';
import { PetGetComponent } from './pet-get/pet-get.component';
import { PetEditComponent } from './pet-edit/pet-edit.component';
import { RouterModule, Routes } from "@angular/router";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PuppysService } from './puppys.service';
import { HeaderComponent } from './header/header.component';

const routes: Routes = [
  {
    path: 'puppy/create',
    component: PetAddComponent
  },
  {
    path: '',
    component: PetGetComponent
  },
  {
    path: 'petEdit',
    component: PetEditComponent
  }
];

@NgModule({
  declarations: [AppComponent, PetAddComponent, PetGetComponent, PetEditComponent, HeaderComponent],
  imports: [BrowserModule,
    RouterModule,
    SlimLoadingBarModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule],
  exports: [RouterModule],
  providers: [PuppysService],
  bootstrap: [AppComponent]
})
export class AppModule { }
