import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class PuppysService {

  uri = 'http://localhost:3000';

  constructor(private http: HttpClient, private router: Router) { }

  addPuppyData(puppy_breed, puppy_age, description) {
    const obj = {
      puppy_breed,
      puppy_age,
      description
    };
    console.log(obj);
    this.http.post(`${this.uri}/createPuppy`, obj)
      .subscribe(res => this.router.navigate(['']));
  }
  getPuppies() {
    return this
      .http
      .get(`${this.uri}/getPuppies`);
  }

  updatePuppy(puppy_breed, puppy_age, description, puppy_id) {

    const obj = {
      puppy_breed: puppy_breed,
      puppy_age: puppy_age,
      description: description
    };
    this
      .http
      .put(`${this.uri}/updatePuppy/${puppy_id}`, obj)
      .subscribe(res => this.router.navigate(['']));
  }
  deletePuppy(puppy_id) {
    return this
      .http
      .delete(`${this.uri}/deletePuppy/${puppy_id}`).subscribe(res => window.location.reload());
  }
}
