import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PuppysService } from '../puppys.service';
@Component({
  selector: 'operation-puppys-pet-add',
  templateUrl: './pet-add.component.html',
  styleUrls: ['./pet-add.component.css']
})
export class PetAddComponent implements OnInit {
  angForm: FormGroup;
  constructor(private fb: FormBuilder, private ps: PuppysService) {
    this.createForm();
  }
  createForm() {
    this.angForm = this.fb.group({
      puppy_breed: ['', Validators.required],
      puppy_age: ['', Validators.required],
      description: ['', Validators.required]
    });
  }
  addPuppy(puppy_breed, puppy_age, description) {
    this.ps.addPuppyData(puppy_breed, puppy_age, description);
  }
  ngOnInit() {
  }

}
