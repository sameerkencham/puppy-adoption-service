import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { PuppysService } from './puppys.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('PuppysService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientModule, RouterTestingModule ]
  }));

  it('should be created', () => {
    const service: PuppysService = TestBed.get(PuppysService);
    expect(service).toBeTruthy();
  });
});
