import { Component, OnInit } from '@angular/core';
import Puppys from '../Puppys';
import { PuppysService } from '../puppys.service';
import { DataShareService } from "../data-share.service";
import { Router } from '@angular/router';

@Component({
  selector: 'operation-puppys-pet-get',
  templateUrl: './pet-get.component.html',
  styleUrls: ['./pet-get.component.css']
})
export class PetGetComponent implements OnInit {
  puppyInfo :Puppys[];

  constructor(private bs: PuppysService, public puppyData: DataShareService, private router: Router) {
    this.bs
      .getPuppies()
      .subscribe((data: any[]) => {
        this.puppyInfo = data["data"];
      });
  }

  ngOnInit() {
    this.bs
      .getPuppies()
      .subscribe((data: any[]) => {
        this.puppyInfo = data["data"];
      });
  }

  updatePuppy(puppy) {
    this.puppyData.changePuppy(puppy);
    this.router.navigate(['petEdit'])
  }

  deletePuppy(puppy_id) {
    this.bs.deletePuppy(puppy_id);
  }

}
