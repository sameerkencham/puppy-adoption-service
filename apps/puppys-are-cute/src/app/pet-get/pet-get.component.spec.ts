import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {ReactiveFormsModule} from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';
import { PetGetComponent } from './pet-get.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('PetGetComponent', () => {
  let component: PetGetComponent;
  let fixture: ComponentFixture<PetGetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetGetComponent ],
      imports: [RouterTestingModule,
        HttpClientModule,
        ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetGetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
